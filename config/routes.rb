Rails.application.routes.draw do
  resources :users
  root 'users#index'

  root 'sessions#new'
  get 'login' => 'sessions#new', as: 'login'
  post 'login' => 'sessions#create'

  delete 'logout' => 'sessions#destroy', as: 'logout'

  get 'signup' => 'users#new', as: 'signup'
  
end
